﻿#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.
#SingleInstance force

#Include ../lib/VA.ahk

Init()

^!+m::
	if (globalDevices.Length() < 2) {
		Init()
	}
	
	device := VA_GetDevice()
	name := VA_GetDeviceName(device)

	currDeviceIndex := 0
	for index, element in globalDevices {
		if (element=name) {
			currDeviceIndex := index
		}
	}

	nextDeviceIndex := Mod(currDeviceIndex + 1, globalDevices.Length() + 1)
	if (nextDeviceIndex = 0) {
		nextDeviceIndex := 1
	}

	newDevice := VA_GetDevice(globalDevices[nextDeviceIndex])
	newDeviceName := VA_GetDeviceName(newDevice)
	VA_SetDefaultEndpoint(newDeviceName, 1)
	Sleep 50
	VA_SetDefaultEndpoint(newDeviceName, 2)

	Sleep 100
	SoundPlay, ../resources/SwitchSound.wav
return

^!+r::
	Init()
return

Init() {
	currentInspectedDevice := 1
	global globalDevices := []
	index := 1
	global BlackList := ""
	FileRead, BlackList, ./Blacklist.txt
	while (true) {
		z := "playback:" index
		currentInspectedDevice := VA_GetDevice(z)
		if (currentInspectedDevice==0) {
			break
		}
		deviceName := VA_GetDeviceName(currentInspectedDevice)
		if (!IsInBlacklist(deviceName, BlackList)) {
			globalDevices.Push(deviceName)
		}
		index := index + 1
	}

	; msgbox % Join("`r`n", globalDevices*)
}

Join(sep, params*) {
	for index,param in params
		str .= param . sep
	return SubStr(str, 1, -StrLen(sep))
}

IsInBlacklist(name, blacklist) {
	blacklist_array := StrSplit(blacklist, "`r`n")
	for index,blacklist_entry in blacklist_array
		if InStr(name, blacklist_entry)
			return true
	return false
}

